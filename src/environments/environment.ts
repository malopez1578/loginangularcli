// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCt5-KtSf1SGanuD2muhStvAEIRdZvGF-Q',
    authDomain: 'malopez1578-c333a.firebaseapp.com',
    databaseURL: 'https://malopez1578-c333a.firebaseio.com',
    projectId: 'malopez1578-c333a',
    storageBucket: 'malopez1578-c333a.appspot.com',
    messagingSenderId: '1092659736800',
    appId: '1:1092659736800:web:f977e2eea8b1b3a9'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
