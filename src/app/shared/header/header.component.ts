import { Component, OnInit } from '@angular/core';
import { AuthService } from '@app/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'aptx-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;                  // {1}

  constructor(private authService: AuthService) {}
  ngOnInit() {
      this.isLoggedIn$ = this.authService.isLoggedIn; // {2}
  }
  logOut() {
    this.authService.logout();
  }
}
