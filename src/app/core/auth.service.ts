import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: Observable<firebase.User>;
  private userDetails: firebase.User = null;
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  nextState(state: any) {
    this.loggedIn.next(state);

  }
  constructor( private angularFireAuth: AngularFireAuth, private router: Router) {
    this.user = this.angularFireAuth.authState;
    this.user.subscribe(
      (user) => {
        if (user) {
          this.userDetails = user;
        } else {
          this.userDetails = null;
        }
      }
      );
      this.isLoggedInExist();
   }
  logIn(email: string, pass: string) {
    return this.angularFireAuth.auth.signInWithEmailAndPassword(email, pass);
  }
  signUp(email: string, pass: string) {
    return this.angularFireAuth.auth.createUserWithEmailAndPassword(email, pass)
    .then((res) => {
      console.log('signUp', res);
    }).catch(error => {
      console.error('signUp', error.message);
    });
  }
  isLoggedInExist() {
    if (this.userDetails == null ) {
        this.loggedIn.next(false);
        return false;
      } else {
        this.loggedIn.next(true);
        return true;
      }
  }
  logout() {
    this.angularFireAuth.auth.signOut()
    .then((res) => this.router.navigate(['']));
  }
}
