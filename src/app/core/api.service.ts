import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class APIService {
  constructor(
    private angularFirestore: AngularFirestore,
    private angularFireStorage: AngularFireStorage,
    private router: Router
  ) {}
  getCollections(table: string) {
    return this.angularFirestore
      .collection(table)
      .snapshotChanges()
      .subscribe(res => {
        res.map((data, i) => {
          console.log(i, data.payload.doc.data());
        });
      });
  }
  addDataUser(table: string, userData: object) {
    return this.angularFirestore.collection(table).add(userData);
  }
  public tareaCloudStorage(nombreArchivo: string, datos: any) {
    return this.angularFireStorage.upload(nombreArchivo, datos);
  }

  // Referencia del archivo

  public referenciaCloudStorage(nombreArchivo: string) {
    return this.angularFireStorage.ref(nombreArchivo);
  }
}
