import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { environment } from '@env/environment';
import { AppRoutingModule } from './app-routing.module';
//  services
import { APIService, ModalService, DomService, AuthService } from '@app/core';
// dependencias firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule, AngularFireStorage } from '@angular/fire/storage';
//  componentes
import { AppComponent } from './app.component';
import { SharedModule } from '@app/shared';

//  directives
import { LoginModalComponent } from './components/modals/login-modal/login-modal.component';

@NgModule({
  declarations: [AppComponent, LoginModalComponent],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule
  ],
  providers: [APIService, ModalService, DomService, AuthService, AngularFireStorage],
  bootstrap: [AppComponent],
  entryComponents: [LoginModalComponent]
})
export class AppModule {}
