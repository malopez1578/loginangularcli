import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'home', loadChildren: () => import('@app/components/home/home.module').then(m => m.HomeModule)},
  { path: 'login', loadChildren: () => import('@app/components/login/login.module').then(m => m.LoginModule)},
  { path: 'registrer', loadChildren: () => import('@app/components/registrer-user/registrer-user.module').then(m => m.RegistrerUserModule)},
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: '**', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
