import { Component, OnInit } from '@angular/core';
import { ModalService } from './core';

@Component({
  selector: 'aptx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private modalService: ModalService) {}
  removeModal() {
    this.modalService.destroy();
  }
  ngOnInit() {
  }
}
