import { Component, OnInit } from '@angular/core';
import { AuthService, ModalService } from '@app/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginModalComponent } from '../modals/login-modal/login-modal.component';
@Component({
  selector: 'aptx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  forma;
  modal: boolean;
  message: string;
  logo = './assets/images/logo.svg';

  modelUser: object = {
    email: '',
    pass: ''
  };

  constructor(
    private authService: AuthService,
    private modalService: ModalService
  ) {}

  ngOnInit() {
    this.forma = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  logIn() {
    const email = this.forma.controls['email'].value;
    const pass = this.forma.controls['password'].value;
    this.authService
      .logIn(email, pass)
      .then(res => {
        this.authService.nextState(true);
        const inputs = {
          data: 'logged successful!!!!!!!'
        };
        this.modalService.init(LoginModalComponent, inputs, {});
        console.log('logIn', res);
      })
      .catch(error => {
        const inputs = {
          data: 'user o password incorrect!!!!!'
        };
        this.modalService.init(LoginModalComponent, inputs, {});
        console.error('logIn', error);
      });
  }
  /**
  * This method is used to get particular data from cache
  * @param string key
  * Pass key for cache
  * @example
  * initLoginModal("user")
  * @returns Promise It returns JSON object
  */
  initLoginModal() {}
}
