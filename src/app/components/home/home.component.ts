import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'aptx-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  logo = './assets/images/logo.svg';

  constructor() {}

  ngOnInit() {}
}
