import { Component, OnInit, Input } from '@angular/core';
import { ModalService } from '@app/core';

@Component({
  selector: 'aptx-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {

  @Input() data: string;
  constructor(private modalService: ModalService) {}

  ngOnInit() {}

  public close() {
    this.modalService.destroy();
  }
}
