import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { RegistrerUserRoutingModule } from './registrer-user-routing.module';
import { RegistrerUserComponent } from './registrer-user.component';

@NgModule({
  declarations: [RegistrerUserComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RegistrerUserRoutingModule
  ]
})
export class RegistrerUserModule { }
