import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginModalComponent } from '../modals/login-modal/login-modal.component';

import { Observable } from 'rxjs';
import { APIService } from '@app/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'aptx-registrer-user',
  templateUrl: './registrer-user.component.html',
  styleUrls: ['./registrer-user.component.scss']
})
export class RegistrerUserComponent implements OnInit {
  task: AngularFireUploadTask;
  formaRegistrer;

  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;
  logo = './assets/images/logo.svg';
  constructor(
    private apiService: APIService,
    private storage: AngularFireStorage
  ) {
  }

  ngOnInit() {
    this.formaRegistrer = new FormGroup({
      photo: new FormControl(null, [Validators.required]),
      nickname: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      birhtday: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      confirm: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }
  upload(event) {
    const id = Math.random()
      .toString(36)
      .substring(2);
    const file = event.target.files[0];
    const filePath = `profile/profile_${id}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
        finalize(() => this.downloadURL = fileRef.getDownloadURL() )
     )
    .subscribe();
  }
}
