import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrerUserComponent } from './registrer-user.component';

const routes: Routes = [
  { path: '', component: RegistrerUserComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrerUserRoutingModule { }
